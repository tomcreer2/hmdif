import re

import pandas as pd
import scipy.interpolate
import numpy as np
from scipy.interpolate import interp1d

def load_in_hmdif(hmdif_fn):
    with open(hmdif_fn, 'r') as f:
        raw_hmdif = f.read()
        f.close()
    
    hmdif_lines = raw_hmdif.split(';')
    
    lines = []
    lines_coords = []
    
    tstart = False
    dstart = False
    
    data_codes = []
    
    currentsectionlabel = ''
    currentstartch= 0
    currentendch = 0
    observ_flag = ''
    sect_props = {}
    headers = {}
    sect_line = []
    
    section_codes = ['LABEL','SNODE','LENGTH','SDATE']
    data_codes = ['LLRT', 'LRRT', 'LLTX', 'LSPD', 'LGRD', 'LFAL', 'LCRV', 'LV3', 'LV10', 'LTRC', 'LWCL', 'LWCR', 'LEDC', 'LEDR', 'LES1', 'LES2', 'LTAD', 'LTRV', 'LLAD', 'LRAD', 'LLRD', 'LRRD', 'LLTD', 'LLTM', 'LLTV', 'LCTM', 'LCTV', 'LRTM', 'LRTV', 'LT05', 'LT95', 'LTVV', 'LL03', 'LL10', 'LLBI', 'LR03', 'LR10', 'LRBI', 'LECR', 'LRCR', 'LSUR', 'LOVD', 'LCOO', 'LMAP']
    
    df = pd.DataFrame(columns=(section_codes+data_codes))
    
    line = {}
    linecoord = {}
    for hmd_line in hmdif_lines:
        linesplit = re.findall(r'[^,;\\\s]+', hmd_line)
        #print(linesplit)
        #break
    
        if len(linesplit) == 0:
            continue
        
        linecode = linesplit[0]
        
    
    
        if linecode == 'TSTART':
            tstart = True
            dstart = False
        elif linecode == 'DSTART':
            tstart = False
            dstart = True
    
            #TODO - create df columns
        elif linecode == 'SECTION':
            if tstart:
                headers['SECTION'] = linesplit[1:]
            elif dstart:
                if line:
                    lines.append(line)
                
                sect_line = re.split('\\\|;|,', hmd_line)
                for x, header in enumerate(headers[linecode]):
                    if x < len(linesplit)-1:
                        line[header] = sect_line[1+x]
                        
                observ_flag = ''
                sect_props = line
        elif linecode =='OBSERV':
            if tstart:
                headers['OBSERV'] = linesplit[1:]
            elif dstart:
                if linesplit[1] not in data_codes:
                    data_codes.append(linesplit[1])
                    
                observ_flag = linesplit[1]
                if (currentsectionlabel != linesplit[2]) or (currentstartch != linesplit[3]) or (currentendch != linesplit[4]):
                    if line:
                        lines.append(line)                
                        
                        line = {}
                        for x, header in enumerate(headers['SECTION']):
                            if x < len(linesplit)-1:
                                line[header] = sect_line[1+x]
                        
                    line['SECTIONLABEL'] = linesplit[2]
                    line['STARTCH'] = linesplit[3]
                    line['ENDCH'] = linesplit[4]
    
                    currentsectionlabel = linesplit[2]
                    currentstartch = linesplit[3]
                    currentendch = linesplit[4]
        elif linecode =='OBVAL':
            if tstart:
                headers['OBVAL'] = linesplit[1:]
            elif dstart:
                if observ_flag == 'LCOO':
                    #LOCATION REFERENCE (OSGB36)
                    #OBVAL\30 X co-ordinates
                    #OBVAL\31 Y co-ordinates
                    #OBVAL\32 Z co-ordinates
                    if linesplit[1] == '30':  
                        for x, header in enumerate(headers['SECTION']):
                            if x < len(linesplit)-1:
                                linecoord[header] = sect_line[1+x]
                                
                        linecoord['SECTIONLABEL'] = line['SECTIONLABEL']
                        linecoord['STARTCH']  = line['STARTCH'] 
                        linecoord['ENDCH'] = line['ENDCH']
                        linecoord['X'] = linesplit[2]
                    elif linesplit[1] == '31':
                        linecoord['Y'] = linesplit[2]
                    elif linesplit[1] == '32':
                        linecoord['Z'] = linesplit[2]
                        if linecoord:
                            lines_coords.append(linecoord)
                            linecoord = {}
                elif observ_flag == 'LMAP':
                    pass
                    #CRACK MAP, treat differently
                    # OBVAL\2 Crack length
                    # OBVAL\23 Offset position
                    # OBVAL\24 Angle
                    # OBVAL\25 Type code (crack or joint)
                else:
                    value = linesplit[2]
                    line[observ_flag] = value
                    #line = [currentsectionlabel, currentstartch, currentendch, observ_flag, value]
                    #df = df.append(dict(sect_props, **{observ_flag : value}) , ignore_index=True)
                    
    df = pd.DataFrame(lines)
    df_coords = pd.DataFrame(lines_coords)
    
    df['nancount'] = df.shape[1] - df.count(axis=1)
    df_scan = df[df['nancount'] < 2]
    
    return [df_scan, df_coords]

def RCI(row, roadclass=None, urban=False):
    if roadclass is None:
        roadclass = row['LABEL'][0:1]
        if roadclass not in 'ABCU':
            roadclass = 'U'
    
    #Taken from UK Roads, SCANNER surveys for Local Roads, User Guide and Specification, Volume 3
    llrt_or_lrrt_scoring = {'code':['LLRT','LRRT'],'A':[10,20], 'B':[10,20], 'C':[10,20],'U':[10,20],'max':100}
    
    lv3_scoring = {'code':'LV3', 'A':[4,10], 'B':[5,13], 'C':[7,17],'U':[8,20],'max':80}
    
    lv10_scoring = {'code':'LV10', 'A':[21,56], 'B':[27,71], 'C':[35,93],'U':[41,110],'max':60}   

    ltrc_scoring = {'code':'LTRC', 'A':[0.15, 2.0], 'B':[0.15, 2.0], 'C':[0.15, 2.0],'U':[0.15, 2.0],'max':60}   

    if urban:
        if roadclass in ['A','B']:
            lltx_scoring = {'code':'LLTX', 'A':[0.6, 0.3], 'B':[0.6, 0.3], 'C':[0.6,0.3],'U':[0.6,0.3],'max':50}   
        else: #C,U
            lltx_scoring = {'code':'LLTX', 'A':[0.6, 0.3], 'B':[0.6, 0.3], 'C':[0.6,0.3],'U':[0.6,0.3],'max':30}   
    else:
        if roadclass in ['A','B']:
            lltx_scoring = {'code':'LLTX', 'A':[0.7, 0.4], 'B':[0.6, 0.3], 'C':[0.6,0.3],'U':[0.6,0.3],'max':75}   
        else: #C,U
            lltx_scoring = {'code':'LLTX', 'A':[0.7, 0.4], 'B':[0.6, 0.3], 'C':[0.6,0.3],'U':[0.6,0.3],'max':50}   
        

    score_funcs = [llrt_or_lrrt_scoring, lv3_scoring, lv10_scoring, ltrc_scoring, lltx_scoring]
    
    rci_score = 0
    for score_func in score_funcs:
        f = scipy.interpolate.interp1d(score_func[roadclass],[0,score_func['max']], kind='linear',fill_value="extrapolate")

        if type(score_func['code']) == list:
            max_code_score = 0                
            for code in score_func['code']:
                code_score = f(row[code])

                code_score = max(0, code_score) #probably a better way to do
                code_score = min(code_score, score_func['max'])
                
                if code_score > max_code_score:
                    max_code_score = code_score
                    
            rci_score = rci_score + max_code_score
        else:
            code_score = f(row[code])
            
            code_score = max(0, code_score)
            code_score = min(code_score, score_func['max'])
        
            rci_score = rci_score + code_score
    
    return rci_score
    #if line:
    #    lines.append(line)


hmdif_fn = 'Data/IOM A CLASS RAV 2019 - CL1.hmd'
hmdif_fns = ['Data/IOM A CLASS RAV 2019 - CL1.hmd', 'Data/IOM A CLASS RAV 2019 - CR1.hmd', 'Data/IOM B CLASS RAV 2019 - CR1.hmd', 'Data/IOM RAV 2019- C CLASS - 120919.hmd', 'Data/IOM B CLASS RAV 2019 - CL1.hmd', 'Data/IOM C CLASS RAV 2019 - CL1.hmd', 'Data/IOM C CLASS RAV 2019 - CR1.hmd', 'Data/IOM RAV 2019- U CLASS.hmd']

df_scan = pd.DataFrame()
df_coords = pd.DataFrame()

for fn in hmdif_fns:    
    [df_scan_tmp, df_coords_tmp] = load_in_hmdif(fn)
    print(fn + ' ' + str(df_scan_tmp.shape[0]) + ' ' + str(df_coords_tmp.shape[0]))
    df_scan = df_scan.append(df_scan_tmp)
    df_coords = df_coords.append(df_coords_tmp)

df_scan['RCI'] = df_scan.apply(lambda x: RCI(x),axis=1)

print("Network in RED condition is {:.0%}".format(df_scan[df_scan['RCI'] >= 100].shape[0]/df_scan.shape[0]))
print("Network in AMBER condition is {:.0%}".format(df_scan[(df_scan['RCI'] < 100) & (df_scan['RCI'] >= 40)].shape[0]/df_scan.shape[0]))
print("Network in GREEN condition is {:.0%}".format(df_scan[df_scan['RCI'] < 40].shape[0]/df_scan.shape[0]))

# %%

df_coords['lengths_fl'] = pd.to_numeric(df_coords['LENGTH'], errors='coerce')
df_coords['roadcode'], df_coords['roadsection'] = df_coords['LABEL'].str.split('_',1).str
df_coords['startchfl'] = pd.to_numeric(df_coords['STARTCH'], errors='coerce')

#df_coords['split_lens'] = pd.to_numeric(df_coords['ENDCH'], errors='coerce') - pd.to_numeric(df_coords['STARTCH'], errors='coerce')
#df_coords['cumlength'] = df_coords.groupby(['roadcode'])['split_lens'].apply(lambda x: x.cumsum())
#df_coords['ch'] = df_coords['cumlength'] - df_coords['lengths_fl'] / 2
#df_labels['X'] = df_labels['ch'].apply(lambda x: df_coords.loc[(df_coords['roadcode']=='A10') & (df_coords['cumlength'] < x)].iloc[-1:]['X'].iloc[0])

def resample(df_scan, target_lengths=100, recode=True, sectionlabel='CL1'):
    if recode:
        #target_lengths=100
        #Split by Roads and work out cumaltive lengths
        #df_labels = df_scan.drop_duplicates(subset='LABEL', keep='first').sort_values('LABEL')[['LABEL','LENGTH','STARTCH','ENDCH','RCI']]
        df_labels = df_scan[df_scan['SECTIONLABEL'] == sectionlabel]
        df_labels['startchfl'] = pd.to_numeric(df_labels['STARTCH'], errors='coerce')
        
        df_labels = df_labels.sort_values(['LABEL','startchfl'])[['LABEL','SECTIONLABEL','LENGTH','STARTCH','ENDCH','RCI']]

        df_labels['roadcode'], df_labels['roadsection'] = df_labels['LABEL'].str.split('_',1).str
        df_labels['lengths_fl'] = pd.to_numeric(df_labels['LENGTH'], errors='coerce')
        df_labels['split_lens'] = pd.to_numeric(df_labels['ENDCH'], errors='coerce') - pd.to_numeric(df_labels['STARTCH'], errors='coerce')

        df_labels = df_labels.reindex()
        df_labels['cumlength'] = df_labels.groupby(['roadcode'])['split_lens'].transform(lambda x: x.cumsum())


        #Create linspace with start & end chainages against RCIs
        df_labels['ch'] = df_labels['cumlength'] - df_labels['lengths_fl']
        df_linspace = df_labels[['roadcode','ch','RCI']]
        df_labels['ch'] = df_labels['cumlength'] - 0.01
        df_linspace = df_linspace.append(df_labels[['roadcode','ch','RCI']])        
        df_linspace = df_linspace.sort_values(by=['roadcode','ch'])
        
        
        roads = df_labels['roadcode'].unique()
        df_resampled = pd.DataFrame()
        for road in roads:
            #Create interpolation function around this
            f1 = interp1d(df_linspace[df_linspace['roadcode']==road]['ch'],df_linspace[df_linspace['roadcode']==road]['RCI'], bounds_error=False)
    
            #Create new 1m-spaced linspace for the whole length and apply interpolation function to fill values
            x1,y1 = df_labels[df_labels['roadcode']==road][['cumlength','LENGTH']].sort_values('cumlength').iloc[0]
            xstart = int(x1) #- int(y1)
            xfin = df_labels[df_labels['roadcode']==road]['cumlength'].max()
            xnew = np.linspace(xstart, xfin, num=(xfin-xstart+1), endpoint=False)
            df_resample = pd.DataFrame(xnew,columns=['ch'])
            df_resample['RCI'] = df_resample['ch'].apply(lambda x: f1(x))
            
            
            #Get average RCI for a section (weighted by lengths inside it)
            #df_resample.loc[(df_resample['ch'] > 10.0) & (df_resample['ch'] < 589.1),'RCI'].mean()
            
            #Create windowed linspace
            num_windows = (xfin-xstart)//target_lengths
            step_size = (xfin-xstart)/num_windows
            xnew2 = np.linspace(float(xstart)+0.5*step_size, float(xfin)-0.5*step_size, num=num_windows, endpoint=True)
            df_target = pd.DataFrame(xnew2,columns=['ch'])
            df_target['RCIavg'] = df_target['ch'].apply(lambda x: df_resample.loc[(df_resample['ch'] > (x-step_size*0.5)) & (df_resample['ch'] <= (x+step_size*0.5)),'RCI'].mean())
            #is df_labels sorted correctly?
            df_target['section'] = df_target['ch'].apply(lambda x: df_labels.loc[(df_labels['roadcode']==road) & (df_labels['cumlength'] < x)].iloc[-1:]['LABEL'].iloc[0])
            df_target['midsectionch'] = df_target['ch'].apply(lambda x: float(df_labels.loc[(df_labels['roadcode']==road) & (df_labels['cumlength'] < x)].iloc[-1:]['STARTCH'].iloc[0]) + x - float(df_labels.loc[(df_labels['roadcode']==road) & (df_labels['cumlength'] < x)].iloc[-1:]['cumlength'].iloc[0]))
            

            def lookup(x, ax, offset=0):
                try:
                    return df_coords.loc[(df_coords['LABEL']==x['section']) & (df_coords['SECTIONLABEL']==sectionlabel) &  (df_coords['startchfl'] < (x['midsectionch'] + offset))].iloc[-1:][ax].iloc[0]
                except:
                    try:
                        if x['midsectionch'] - offset < step_size:
                            return df_coords.loc[(df_coords['LABEL']==x['section']) & (df_coords['SECTIONLABEL']==sectionlabel) &  (df_coords['startchfl'] > (x['midsectionch']))].iloc[:1][ax].iloc[0]
                        else:
                            return 0.0
                    except:
                        return 0.0
                                    
            df_target['X1str'] = df_target.apply(lambda x: lookup(x, 'X', offset=(step_size/2-10)), axis=1)      
            df_target['Y1str'] = df_target.apply(lambda x: lookup(x, 'Y', offset=(step_size/2-10)), axis=1)
            df_target['X2str'] = df_target.apply(lambda x: lookup(x, 'X', offset=-(step_size/2-10)), axis=1)     
            df_target['Y2str'] = df_target.apply(lambda x: lookup(x, 'Y', offset=-(step_size/2-10)), axis=1)
            df_target['X1'] =  pd.to_numeric(df_target['X1str'], errors='coerce')
            df_target['Y1'] =  pd.to_numeric(df_target['Y1str'], errors='coerce')
            df_target['X2'] =  pd.to_numeric(df_target['X2str'], errors='coerce')
            df_target['Y2'] =  pd.to_numeric(df_target['Y2str'], errors='coerce')
            df_target['SECTIONLABEL'] = sectionlabel
            df_resampled = df_resampled.append(df_target)
        return df_resampled
    else:
        pass
        
df_resampled = pd.DataFrame()
for sectionlabel in ['CL1','CR1']:
    df_resampled = df_resampled.append(resample(df_scan,sectionlabel=sectionlabel, target_lengths=100))#,sectionlabel=sectionlabel))